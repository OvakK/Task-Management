export const USER_MESSAGES = {
  USER_NAME_EXISTS: {
    message: 'err_user_name_exists',
  },
  USER_EMAIL_EXISTS: {
    message: 'err_user_email_exists',
  },
  PASSWORD_NOT_MATCH: {
    message: 'err_user_email_or_password',
  },
  USER_NOT_EXISTS: {
    message: 'err_user_not_exists',
  },
  EMPTY_BODY: {
    message: 'err_empty_updated_body',
  },
};

export const TitleValue = {
  title: 'Auth',
  login: 'User Login',
  register: 'User Register',
  updatePassword: 'Update Password'
} as const;

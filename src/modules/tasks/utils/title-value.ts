export const TitleValue = {
  title: 'Tasks',
  createTask: 'Create Task',
  getTask: 'Get Tasks',
  updateTask: 'Update Task',
  deleteTask: 'Delete Task',
  filterTask: 'Filter Task',
  adminAccess: 'Admin Access',
  userAccess: 'User Access',
} as const;
